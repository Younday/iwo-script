#!/bin/bash

COUNT=0
#Loop through every file containg tweets on the 27th of April 2016
for file in /net/corpora/twitter2/Tweets/2016/04/20160427:*; do
  #Store the amount of tweets containing the words in question in a variable
  COUNTER=`zless $file | /net/corpora/twitter2/tools/tweet2tab -i text | python3 process_tweets.py`
  #Print filename and count
  echo $file
  echo $COUNTER
  #Add the count of the hour to the total count of the day
  COUNT=$[$COUNT + $COUNTER]
done;
echo "Total count of tweets containing the words 'bier' or 'pils' on Kingsday"
#Print the total count of the day
echo $COUNT

COUNT1=0
for file in /net/corpora/twitter2/Tweets/2016/02/20160208:*; do
  COUNTER=`zless $file | /net/corpora/twitter2/tools/tweet2tab -i text | python3 process_tweets.py`
  echo $file
  echo $COUNTER
  COUNT1=$[$COUNT1 + $COUNTER]
done;
echo "Total count of tweets containing the words 'bier' or 'pils' on the second day of Carnaval"
echo $COUNT1

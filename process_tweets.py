#!/usr/bin/python3

import sys

def find_tweet(line):
    item_list = ["bier", "pils", "Bier", "Pils", "BIER", "PILS"]
    count = 0
    for item in item_list:
        if item in line:
            count = count + 1
    return count

def main():
    count = 0
    for line in sys.stdin:
        count = count + find_tweet(line)
    print(count)

if __name__ == '__main__':
    main()
